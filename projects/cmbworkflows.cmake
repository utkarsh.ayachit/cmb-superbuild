superbuild_add_project(cmbworkflows
  CONFIGURE_COMMAND
    ""
  BUILD_COMMAND
    ""
  INSTALL_COMMAND
    "${CMAKE_COMMAND}"
      -Dsource_dir:PATH=<SOURCE_DIR>
      -Dinstall_dir:PATH=<INSTALL_DIR>
      -P "${CMAKE_CURRENT_LIST_DIR}/scripts/cmbworkflows.install.cmake")

superbuild_add_extra_cmake_args(
  -DSIMULATION_WORKFLOWS_ROOT:PATH=<INSTALL_DIR>/share/cmb/workflows)
