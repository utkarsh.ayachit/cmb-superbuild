# Capstone is a private program to which Kitware developers have access. We
# provide logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
superbuild_add_project(capstone
  MUST_USE_SYSTEM)
