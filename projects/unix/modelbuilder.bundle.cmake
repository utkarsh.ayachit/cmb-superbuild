set(cmb_doc_dir "share/cmb/doc")
set(plugin_dir "lib")

include(modelbuilder.bundle.common)
include(cmb.bundle.unix)

# Install PDF guides.
cmb_install_extra_data()
