cmake_minimum_required(VERSION 3.6.1)

project(meshkit-superbuild)

function (superbuild_find_projects var)
  set(projects
    cgm
    cxx11
    eigen
    hdf5
    meshkit
    moab
    netcdf
    szip
    zlib)

  set("${var}"
    ${projects}
    PARENT_SCOPE)
endfunction ()

function (superbuild_sanity_check)
  if (NOT meshkit_enabled)
    message(FATAL_ERROR "Meshkit is disabled...")
  endif ()
endfunction ()

list(APPEND superbuild_version_files
  "${CMAKE_CURRENT_LIST_DIR}/versions.cmake"
  "${CMAKE_CURRENT_LIST_DIR}/../versions.cmake")
list(APPEND superbuild_project_roots
  "${CMAKE_CURRENT_LIST_DIR}/projects"
  "${CMAKE_CURRENT_LIST_DIR}/../projects")

# set the default arguments used for "git clone"
set(_git_clone_arguments_default --progress)

# set the default for meshkit to be enabled for this project
set(_superbuild_default_meshkit ON)

if (NOT EXISTS "${CMAKE_CURRENT_LIST_DIR}/../superbuild/CMakeLists.txt")
  message(FATAL_ERROR "It appears as though the superbuild infrastructure "
                      "is missing; did you forget to do `git submodule init` "
                      "and `git submodule update`?")
endif ()

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../superbuild" "${CMAKE_CURRENT_BINARY_DIR}/superbuild")
