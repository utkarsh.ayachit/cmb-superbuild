macro (cmb_superbuild_add_pdf name outname)
  superbuild_add_project("${name}"
    DOWNLOAD_NO_EXTRACT 1
    DEFAULT_ON
    CONFIGURE_COMMAND
      ""
    BUILD_COMMAND
      ""
    INSTALL_COMMAND
      "${CMAKE_COMMAND}" -E copy_if_different
        <DOWNLOADED_FILE>
        "<INSTALL_DIR>/doc/${outname}")

  if (${name}_enabled)
    set("${name}_pdf" "${superbuild_install_location}/doc/${outname}")
  endif ()
endmacro ()

set_property(GLOBAL
  PROPERTY
    cmb_superbuild_lfs_steps
      lfs-init
      lfs-download
      lfs-checkout)

macro (cmb_superbuild_add_lfs_steps _name)
  superbuild_project_add_step(lfs-init
    COMMAND   "${GIT_EXECUTABLE}"
              lfs
              install
              --local
    DEPENDEES download
    DEPENDERS configure
    DEPENDS   ${ARGN}
    COMMENT   "Enabling LFS for ${_name}"
    WORKING_DIRECTORY <SOURCE_DIR>)

  superbuild_project_add_step(lfs-download
    COMMAND   "${GIT_EXECUTABLE}"
              lfs
              fetch
    DEPENDEES lfs-init
    DEPENDERS configure
    COMMENT   "Downloading LFS data for ${_name}"
    WORKING_DIRECTORY <SOURCE_DIR>)

  superbuild_project_add_step(lfs-checkout
    COMMAND   "${GIT_EXECUTABLE}"
              lfs
              checkout
    DEPENDEES lfs-download
    DEPENDERS configure
    COMMENT   "Checking out LFS data for ${_name}"
    WORKING_DIRECTORY <SOURCE_DIR>)
endmacro ()
