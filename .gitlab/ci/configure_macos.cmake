set(ENABLE_python2 OFF CACHE BOOL "")
set(ENABLE_python3 ON CACHE BOOL "")

set(CMAKE_OSX_DEPLOYMENT_TARGET "10.14" CACHE STRING "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
